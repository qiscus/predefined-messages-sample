# Predefined Messages in Qiscus Multichannel

Qiscus Multichannel memiliki fitur Predefined Messages di mana anda dapat mengirim rich messages kepada customer anda. Fitur ini bisa digunakan dengan mengintegrasikan API yang anda miliki. Dokumentasi detail terkait penggunaan fitur ini dapat anda lihat pada halaman ini: https://documentation.qiscus.com/latest/multichannel-customer-service/settings-and-configuration#predefined-messages.

## Requirements

- Untuk menjalankan sample code ini, anda harus menginstall Python version >= 3.7
- Ngrok (atau sejenisnya) untuk tunnel localhost
- Qiscus Multichannel account

## Quickstart

**Clone repository ini**

```sh
git clone https://nurcholisart@bitbucket.org/qiscus/predefined-messages-sample.git
```

**Masuk ke direktori repo**

```bash
cd predefined-messages-sample
```

**Install packages**

```bash
pip install -r requirements.txt
```

**Jalankan server**

```bash
uvicorn main:app --reload
```

**Buka terminal baru dan jalankan ngrok**

```bash
ngrok http 8000
```

**Daftarkan Ngrok URL anda di Qiscus Multichannel**

![](https://d1edrlpyc25xu0.cloudfront.net/abi-jpvsoguhpv18uuoem/image/upload/c9K3RPDuUx/predefined-messages-setup.gif)

```bash
URL ngrok bisa anda dapatkan setelah run command diatas. Contoh URL Ngrok:
https://{id}.ngrok.io
```

**Terakhir, buka room dengan channel Qiscus. dan klik tombol Predefiend Messages**

![](https://d1edrlpyc25xu0.cloudfront.net/abi-jpvsoguhpv18uuoem/image/upload/8eHY6BZEZh/predefined-messages.gif)