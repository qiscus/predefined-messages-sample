from typing import Optional
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from resources import products

app = FastAPI()

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/p")
@app.get("/")
def root(keyword: Optional[str] = ""):
    if keyword.strip() != "":
        filtered_products = []
        for product in products:
            if keyword in product["title"].lower():
                filtered_products.append(product)

        return filtered_products
    else:
        return products
